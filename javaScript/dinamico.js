
//ejercicio1
$("#boton1").click(function(){
    $("#id1").append("Algo... <br>");
});

//ejercicio2
$("#boton2").click(function(){
    var lista=["Segundo","Tercero","Cuarto","Quinto","Sexto"];
    $(lista).each(function(item){
     $("#listaItems").append("<li>"+ lista[item] +"</li>");
    });
});

//ejercicio3
$("#boton3").click(function(){
    var listaTelefonos = [{
        nombre: "ana",
        tel: "android"
    },
    {
        nombre: "javi",
        tel: "iphone"
    },
    {
        nombre: "carmen",
        tel: "android"
    },
    {
        nombre: "ruben",
        tel: "android"
    },
    ];
    $(listaTelefonos).each(function(i,item){
         $("table#agenda tbody").append("<tr><td>"+item.nombre+"</td><td>"+item.tel+"</td></tr>");
    });
});

//ejercicio 4
var and = 0;
var iph = 0;
var otr = 0;
$("#boton4").click(function(){
    $.getJSON("clientes.json", function (data) {
        data.clientes.forEach(function (item) {
            $("table#clientes tbody").append("<tr><td class=ids>"+item.id+"</td><td>"+item.nombre+"</td><td class=tele>"+item.tel+"</tr>");
            if (item.tel == "android"){
                and ++;
            }
            else if (item.tel == "iphone"){
                iph ++;
            }
            else {
                otr ++;
            }
        });
        var andto = (and / (and + iph + otr) * 100).toFixed(2);
        var iphto = (iph / (and + iph + otr) * 100).toFixed(2);
        var otrto = (otr / (and + iph + otr) * 100).toFixed(2);
        $("table#clientes tfoot").append("<tr><td>Android</td><td>"+and+"</td><td>"+andto+"%</td></tr><tr><td>Iphone</td><td>"+iph+"</td><td>"+iphto+"% </td></tr><tr><td>Otros</td><td>"+otr+"</td><td>"+otrto+"%</td></tr>");
    });
});

$("#boton5").click(function(){
    var buscarId = Number ($("input.texto3").val());
    var comp = false;
    $.getJSON("clientes.json", function (data) {
        data.clientes.forEach(function (item) {
            if (item.id == buscarId){
                $("table#resultado tbody").append("<table><tr><th>ID </th><td>"+item.id+"</td></tr><tr><th>Nombre</th><td>"+item.nombre+"</td></tr><tr><th> Telefono </th><td>"+item.tel+"</td></tr><tr><th> Email </th><td>"+item.email+"</td></tr></table>");  
                comp = true;
                $("input.texto3").css("background-color", "white");
            }
        });
        if (comp == false){
            $("input.texto3").css("background-color", "red");
        }
    });
});

